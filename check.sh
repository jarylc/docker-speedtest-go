#!/bin/bash

apk add curl jq

[[ ! -f EXISTING ]] || touch EXISTING
EXISTING=$(cat EXISTING)
echo "Existing: ${EXISTING}"

if [[ -n $OVERWRITE ]]; then
  echo "Overwriting: $OVERWRITE"
  LATEST=$OVERWRITE
else
  LATEST=$(curl -ks https://api.github.com/repos/librespeed/speedtest-go/git/refs/tags | jq -r '.[-1].ref' | cut -d'/' -f3)
  echo "Latest speedtest-go: ${LATEST}"
fi

if [[ (-n "${LATEST}" && "${LATEST}" != "${EXISTING}") ]]; then
  mv build.template.yml build.yml
  sed -i "s \$LATEST ${LATEST} g" 'build.yml'

  echo "Building..."
fi
