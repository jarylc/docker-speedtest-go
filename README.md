![](https://images.microbadger.com/badges/version/jarylc/librespeed-speedtest-go.svg) ![](https://images.microbadger.com/badges/image/jarylc/librespeed-speedtest-go.svg) ![](https://img.shields.io/docker/stars/jarylc/librespeed-speedtest-go.svg) ![](https://img.shields.io/docker/pulls/jarylc/librespeed-speedtest-go.svg)

[Configuration](https://github.com/librespeed/speedtest-go/blob/master/README.md)

# Environment variables:
| Environment | Default value | Description        |
|-------------|---------------|--------------------|
| UID         | 1000          | User ID to run as  |
| GID         | 1000          | Group ID to run as |
| TZ          | UTC           | Timezone           |

# Volumes
- /config - contains [settings.toml](https://github.com/librespeed/speedtest-go/blob/master/settings.toml), optional as image is packaged with working defaults
- /app/assets - contains [assets](https://github.com/librespeed/speedtest-go/tree/master/web/assets), optional as image is packaged with working defaults

# Deploying
## Terminal
```bash
docker run -d \
    --name speedtest \
    -e UID=1000 \
    -e GID=1000 \
    -e TZ=UTC \
    -p 8989:8989 \
    -v /path/config:/config \
    -v /path/assets:/app/assets \
    --restart unless-stopped \
    minimages/librespeed-speedtest-go
```
## Docker-compose
```yml
speedtest:
    image: minimages/librespeed-speedtest-go
    ports:
        - "8989:8989"
    volumes:
        - /path/config:/config
        - /path/config:/app/assets
    environment:
        - UID=1000
        - GID=1000
        - TZ=UTC
    restart: unless-stopped
```
